import { Component, OnInit, Input } from '@angular/core';
import {BasicModel} from './basic-model';
import * as $ from 'jquery'
@Component({
  selector: 'app-basic-details',
  templateUrl: './basic-details.component.html',
  styleUrls: ['./basic-details.component.css']
})
export class BasicDetailsComponent implements OnInit {
  @Input() item:string;
  list:BasicModel[]=[];
  selectedFile: File;
  detailsModal:any={
    name:"",
    list:[]
  };
  constructor() { 
    this.defaultData();
  }

  ngOnInit() {
    // console.log(this.item);
    // let m:BasicModel;
    // m=new BasicModel();
    // this.list.push(m);    
  }
  addMore(){
    let m:BasicModel=new BasicModel();
    m.id=0;
    m.name="";
    m.mobile="";
    m.image="";
    this.list.push(m);
  }
  
  onFileChanged(event,detail) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        detail.image = reader.result;
      }
    }
  }
  defaultData(){
    let m:BasicModel=new BasicModel();
    m.id=1;
    m.name="BosH";
    m.mobile="9849501808";
    m.image="";
    


    this.list.push(m);    
    let m1:BasicModel=new BasicModel();
    m1.id=1;
    m1.name="Ramana";
    m1.mobile="9999999999";
    m1.image="";
    this.list.push(m1);    

  }
  save(){
    $(document).ready(function () {
      alert("Hello World");
  });
  
  }
}
