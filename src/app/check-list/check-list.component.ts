import { Component, ComponentRef, OnInit, ComponentFactoryResolver,ViewChild, ViewContainerRef, Output } from '@angular/core';
import {BasicDetailsComponent} from '../basic-details/basic-details.component';
import { DepartmentDetailsComponent} from '../department-details/department-details.component';
// import * as $ from 'jquery';
declare var $:any;
declare var jQuery:any;
@Component({
  selector: 'app-check-list',
  templateUrl: './check-list.component.html',
  styleUrls: ['./check-list.component.css']
})
export class CheckListComponent implements OnInit {

  //@ViewChild("firstNameInput") firstnameBox; and when submit i remove manually all the classes with: this.firstNameInput.nativeElement.classList.remove("valid"); 


  @ViewChild('groupContainerRef', {read: ViewContainerRef}) VCR: ViewContainerRef;
  model:any={};
  option:any;
  @Output() itemName:string;
  constructor(private CFR: ComponentFactoryResolver) { 
    //test  
    this.option="basic";
  }

  ngOnInit() {
    $(function(){
      $('select').material_select();
    }); // end of document ready
  }

  ngAfterViewInit(){
    this.option="basic";
    this.change();
  }
  login(){
    console.log(JSON.stringify(this.model));
  }
  change(){
    if(this.VCR)
    if(this.VCR.length>0)
      this.VCR.remove(0);

    let componentFactory;
    if(this.option=="basic"){
      //this.itemName="Basic Details";
      componentFactory= this.CFR.resolveComponentFactory(BasicDetailsComponent);
      let componentRef: ComponentRef<BasicDetailsComponent> = this.VCR.createComponent(componentFactory);
      componentRef.instance.item="Basic Details From Parent!!!";
      componentRef.changeDetectorRef.detectChanges();
    }else if(this.option=="department"){
      this.itemName="Department Details";
      componentFactory= this.CFR.resolveComponentFactory(DepartmentDetailsComponent);
      let componentRef: ComponentRef<DepartmentDetailsComponent> = this.VCR.createComponent(componentFactory);
      //componentRef.instance.item="Department Details";
      componentRef.changeDetectorRef.detectChanges();
    }
  }
}
